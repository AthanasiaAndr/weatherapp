# Weatherable

This project was created with Angular version 11.1.0 and its main purpose is for a user to be able to monitor the weather of multiple cities.

## Code structure

- Application starts in app folder which is divided in three folders:
  - components: includes all components that app uses, with their styles files.
  - services: include service to communicate with APIs.
  - shared: includes all shared functionality such as interfaces and pipes for displaying data transformation.
- Assets folder includes all icons for application, divided in app icons and weather icons.
- In styles.sccs there are general style rules and media queries for small devices.
- In the enviroments folder there are API parameters, such as API keys and API endpoints.

## How it works

User types a city name in the input field and when he presses submit, a request is sent through a service, to the OpenPosition API.
When first request completes, a nested request to OpenWeather API is triggerd, with parameters the latitude and longitude retrieved from the OpenPosition API response.
When all requests are completed, a DTO is provided to component and displayed.

## Getting started

Install the npm packages described in the package.json by running `npm install`.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

This application uses PositionStack API (https://positionstack.com/) and OpenWeather API (https://openweathermap.org/api) for weather data.
Follow links to get API keys and then go to enviroments folder and add them like you see below.

```
export const environment = {
  ...
  positionApiKey: 'YOUR_POSITION_WEATHER_API_KEY',
  weatherApiKey: 'YOUR_OPEN_WEATHER_API_KEY'
};
```

When you are ready with your keys you can search a city you like to see and selected to include in your list.
