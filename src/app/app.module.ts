import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchCityComponent } from './componets/search-city/search-city.component';
import { CelsiusTempPipe } from './shared/pipes/celsius-temp.pipe';
import { DecimalPipe } from '@angular/common';
import { WeatherIconPipe } from './shared/pipes/weather-icon.pipe';
import { AppIconPipe } from './shared/pipes/app-icon.pipe';
import { CitiesListComponent } from './componets/cities-list/cities-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchCityComponent,
    CelsiusTempPipe,
    WeatherIconPipe,
    AppIconPipe,
    CitiesListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [DecimalPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
