import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { WeatherCity } from '../shared/models/weather-city';

@Injectable({
  providedIn: 'root'
})
export class CitiesSearchService {

  constructor(private httpClient: HttpClient) { }

  searchCity(query: string): Observable<WeatherCity> {
    const url = `${environment.positionApi}?access_key=${environment.positionApiKey}&query=${query}`;
    return this.httpClient.get(url).pipe(
      mergeMap((positionData: any) => {
        return this.getCityWeather(positionData.data[0].latitude, positionData.data[0].longitude).pipe(
          map((weatherData) => {
            const dataObj: WeatherCity = {
              temperature: weatherData.main.temp,
              weatherDescription: weatherData.weather[0].description,
              city: positionData.data[0].label,
              latitude: positionData.data[0].latitude,
              longitude: positionData.data[0].longitude,
              icon: weatherData.weather[0].icon
            };

            return dataObj;
          })
        );
      })
    );
  }


  getCityWeather(latitude: any, longitude: any): Observable<any> {
    const url = `${environment.weatherApi}?lat=${latitude}&lon=${longitude}&appid=${environment.weatherApiKey}`;
    return this.httpClient.get(url);
  }

}
