import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { WeatherCity } from 'src/app/shared/models/weather-city';

@Component({
  selector: 'app-cities-list',
  templateUrl: './cities-list.component.html',
  styleUrls: ['./cities-list.component.scss']
})
export class CitiesListComponent implements OnInit {
@Input() selectedCities: WeatherCity[];

  constructor() { }

  ngOnInit(): void {
  }

}
