import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search-city',
  templateUrl: './search-city.component.html',
  styleUrls: ['./search-city.component.scss'],
})
export class SearchCityComponent implements OnInit {
  @Output() submitCity = new EventEmitter<string>();

  searchControl = new FormControl('');

  constructor() { }

  ngOnInit(): void {}

  onSubmit(): void {
    if (this.searchControl.value) {
      this.submitCity.emit(this.searchControl.value);
      this.searchControl.setValue(null);
    }
  }
}
