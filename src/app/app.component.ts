import { Component } from '@angular/core';
import { CitiesSearchService } from './services/cities-search.service';
import { WeatherCity } from './shared/models/weather-city';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  selectedCities: WeatherCity[] = [];

  constructor(private citiesSearchSrv: CitiesSearchService) {}

  onSubmitCity(query: string): void {
    this.citiesSearchSrv.searchCity(query).subscribe(cityData => {
      this.selectedCities.push(cityData);
    }, (error) => {
      window.alert('Something went wrong!');
    });

  }
}
