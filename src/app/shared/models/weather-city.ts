export interface WeatherCity {
    temperature: number;
    weatherDescription: string;
    city: string;
    latitude: number;
    longitude: number;
    icon: string;
}