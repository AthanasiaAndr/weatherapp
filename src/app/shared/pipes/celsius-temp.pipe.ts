import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'celsiusTemp'
})
export class CelsiusTempPipe implements PipeTransform {

  transform(value: number, ...args: any[]): number {

    return value - 273.15;
  }

}
