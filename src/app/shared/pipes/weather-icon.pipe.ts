import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weatherIcon'
})
export class WeatherIconPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {

    return `../../../assets/icons/weather/${value}.png`;
  }

}
