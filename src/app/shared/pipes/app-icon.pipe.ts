import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appIcon'
})
export class AppIconPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    return `../../../assets/icons/app-icons/${value}.png`;
  }

}
